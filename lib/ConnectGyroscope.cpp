#include "ConnectGyroscope.hpp"

bool InitGyroscope()
{
	if(ReceiveGyroscope(WHO_AM_I) == I_AM)
	{
		// デバイスの認識を確認
		//SendGyroscope(CTRL_REG_1, (DATA_RATE << 4) | (LOW_POWER_ENABLE << 3) | 0x07);	//XYZ軸全ての加速度センサーを有効化、またデータレートの設定。
		//SendGyroscope(CTRL_REG_1, 0x27);
		//SendGyroscope(CTRL_REG_4, 0x08);												//High resolutionモードにする。

		return true;
	}
	else
	{
		return false;
	}
}

bool SendGyroscope(uint8_t sub_address, uint8_t data)
{
	if(StartI2C(ADDRESS | WRITE) == true)
	{
		WriteData(sub_address);
		WriteData(data);
		StopI2C();
	}
}

uint8_t ReceiveGyroscope(uint8_t sub_address)
{
	// デバイスの認識
	if(StartI2C(ADDRESS | WRITE) == true)
	{
		// 書き込むレジスタの指定
		WriteData(sub_address);
		PORTB = 0x03;

		// 再送開始条件
		if(StartI2C(ADDRESS | READ) == true)
		{
			// データを取り、NACKを返す。
			const uint8_t receive_data = ReceiveNack();
			PORTB = 0x01;
			StopI2C();
			return receive_data;
		}
		else
		{
		}

	}
	else
	{

	}
}

int main()
{
	InitI2C(100000);
	DDRB = 0xff;

	if(InitGyroscope() == true)
	{
		PORTB = 0xff;
	}
	else
	{
		PORTB = 0b10101010;
	}

	while(1)
	{

	}
}
