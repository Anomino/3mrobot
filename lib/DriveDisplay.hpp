/***********************************
 * 秋月電子のI2C液晶接続ライブラリ *
 * *********************************/

#include <avr/io.h>
#include <util/delay.h>
#include "../lib/I2C.h"
#include "DriveDisplay_config.hpp"

bool InitDisplay();
bool SendCommandDisplay(uint8_t command);
bool SendDataDisplay(uint8_t data);
bool SendTextDisplay(char *text);
