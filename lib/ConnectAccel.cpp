#include "ConnectAccel.hpp"

bool InitAccel()
{
	if(ReceiveAccel(WHO_AM_I) == I_AM)
	{
		// デバイスの認識を確認
		//SendAccel(CTRL_REG_1, (DATA_RATE << 4) | (LOW_POWER_ENABLE << 3) | 0x07);	//XYZ軸全ての加速度センサーを有効化、またデータレートの設定。
		SendAccel(CTRL_REG_1, 0x27);
		SendAccel(CTRL_REG_4, 0x08);												//High resolutionモードにする。

		return true;
	}
	else
	{
		return false;
	}
}

bool SendAccel(uint8_t sub_address, uint8_t data)
{
	if(StartI2C(ADDRESS | WRITE) == true)
	{
		WriteData(sub_address);
		WriteData(data);
		StopI2C();
	}
}

uint8_t ReceiveAccel(uint8_t sub_address)
{
	// デバイスの認識
	if(StartI2C(ADDRESS | WRITE) == true)
	{
		// 書き込むレジスタの指定
		WriteData(sub_address);

		// 再送開始条件
		if(StartI2C(ADDRESS | READ) == true)
		{
			// データを取り、NACKを返す。
			const uint8_t receive_data = ReceiveNack();
			return receive_data;
		}
		else
		{

		}

	}
	else
	{

	}
}

double ReceiveXAccel()
{
	const uint8_t out_x_l = ReceiveAccel(OUT_X_L);
	const uint8_t out_x_h = ReceiveAccel(OUT_X_H);

	const double x_accel = (out_x_h << 8 | out_x_l) >> 4;

	return x_accel;
}

double ReceiveYAccel()
{
	const uint8_t out_y_l = ReceiveAccel(OUT_Y_L);
	const uint8_t out_y_h = ReceiveAccel(OUT_Y_H);

	const double y_accel = (out_y_h << 8 | out_y_l) >> 4;

	return y_accel;
}

double ReceiveZAccel()
{
	const uint8_t out_z_l = ReceiveAccel(OUT_Z_L);
	const uint8_t out_z_h = ReceiveAccel(OUT_Z_H);
	
	const double z_accel = (out_z_h << 8 | out_z_l) >> 4;

	return z_accel;
}

/*
// 動作テスト
int main()
{
	DDRB = 0x81;
	DDRD = 0xe0;

	InitI2C(80000);

	InitAccel();

	while(1)
	{
		while((ReceiveAccel(STATUS_REG) & 0x80) == 0);
		double x_accel = ReceiveXAccel();
		double y_accel = ReceiveYAccel();
		double z_accel = ReceiveZAccel();

		if(x_accel > 0)
		{
			PORTD = 0x20;
		}
		else
		{
			PORTD &= ~0x20;
		}

		if(y_accel > 0)
		{
			PORTB = 0x80;
		}
		else
		{
			PORTB &= ~0x80;
		}
		if(z_accel > 0)
		{
			PORTB = 0x01;
		}
		else
		{
			PORTB &= ~0x01;
		}

	}
}
*/
