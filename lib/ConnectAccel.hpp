/********************************************************
 * 秋STマイクロ社製の３軸加速度センサー接続用ライブラリ *
 * ******************************************************/

#pragma once

#include <avr/io.h>
#include "../lib/I2C.h"
#include "ConnectAccel_config.hpp"

bool InitAccel();				//加速度センサーの初期化 正常であればtrueを返す。加速度範囲は、+-2g
bool SendAccel(uint8_t sub_address, uint8_t data);	//加速度センサーへのデータの送信(1バイト)
uint8_t ReceiveAccel(uint8_t sub_address);			//加速度センサーからのデータの受信(1バイト)
double ReceiveXAccel();			//X軸加速度を返す。単位はg
double ReceiveYAccel();			//Y軸加速度を返す。単位はg
double ReceiveZAccel();			//Z軸加速度を返す。単位はg
