/******************************
 * 3mちょうどで止まるロボット *
 * ****************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "3mRobot_config.hpp"
#include "../lib/ConnectAccel.hpp"
#include "../lib/ConnectGyroscope.hpp"

static const double dt = 0.0064;		//積分間隔

static double x_accel = 0,		//X軸加速度
			  y_accel = 0,		//Y軸加速度

			  x_speed = 0,		//X軸速度
			  y_speed = 0,		//Y軸速度

			  x_distance = 0,	//X軸現在位位置
			  y_distance = 0;   //Y軸現在位位置

// タイマー0のコンペアマッチイベントハンドラ
ISR(TIMER2_COMPA_vect)
{
	TCNT2 = 0;

	// 得た加速度(X,Y軸のみ)を足して、積分していく。
	x_accel = ReceiveXAccel() / 1024 * G_ACCEL;
	y_accel = ReceiveYAccel() / 1024 * G_ACCEL;

	if(x_accel < 0.02)
	{
		x_accel = 0;
		SET_LED_1;
	}
	else
	{
		RESET_LED_1;
	}

	if(y_accel < 0.02)
	{
		y_accel = 0;
		SET_LED_3;
	}
	else
	{
		RESET_LED_3;
	}
	
	x_speed += x_accel * dt;
	y_speed += y_accel * dt;

	x_distance += x_speed * dt;
	y_distance += y_speed * dt;
}

// 各種ポートの入出力方向設定
void InitPort()
{
	INIT_LED_1;
	INIT_LED_2;
	INIT_LED_3;
}

// タイマー2の設定
void InitTimer()
{
	TCCR2A = 0x03;	//TOP値固定
	TCCR2B = 0x07;
	OCR2A  = 50;	//6.4ms 0.0064sごとのコンペアマッチ
	TIMSK2 = 0x02;
}

int main()
{
	InitPort();
	InitTimer();
	InitI2C(100000);
	
	sei();
	const bool check_accel = InitAccel();
	//const bool check_mdc   = InitMDC(MDC_ADDRESS);

	/*	
	if(check_mdc == false)
	{
		//MDCと正常に接続することができなかった
		SET_LED_2(LED_ON);
	}
	*/
	while(1)
	{
		if((x_distance >= TARGET_DISTANCE) || (y_distance >= TARGET_DISTANCE))
		{
			// X軸、またY軸が目標距離に到達したので、モータを止める。
			RESET_LED_2;
		}
		else
		{
			SET_LED_2;
		}
	
		// モータを回転させる。
		
	}

}
