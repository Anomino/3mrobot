/**********************************
 * 開発したMotorDriver Normal * 2 *
 * の制御をするライブラリ         *
 * ********************************/

#pragma once

#include <avr/io.h>
#include "SoftwarePwm.h"
#include "I2C.h"

bool InitMDC(uint8_t address);										//このデバイスへの接続をする。正常であればtrueを返す。
bool DriveMotor(uint8_t motor, uint8_t mode, uint8_t duty = 100);	//モータを駆動させる関数
