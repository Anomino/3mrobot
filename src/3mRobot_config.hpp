/************************************************
 * 3mロボットに搭載されているコンピュータの設定 *
 * **********************************************/

#pragma once

#define TARGET_DISTANCE	0.3		//目標距離。単位はm

#define G_ACCEL			9.8		//重力加速度

#define MDC_ADDRESS		0x31	//MDCのデバイスアドレス

#define LED_ON
#define LED_OFF

#define INIT_LED_1			DDRB |= 0x80
#define INIT_LED_2			DDRB |= 0x10
#define INIT_LED_3			DDRD |= 0x80

#define SET_LED_1			PORTB |= 0x80
#define SET_LED_2			PORTB |= 0x10
#define SET_LED_3			PORTD |= 0x80

#define RESET_LED_1			PORTB &= ~0x80
#define RESET_LED_2			PORTB &= ~0x10
#define RESET_LED_3			PORTD &= ~0x80
