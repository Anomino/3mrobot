#pragma once

#define READ		1
#define WRITE		0

#define DATA_RATE		0
#define LOW_POWER_ENABLE 0

#define G_ACCEL			9.8


// レジスタのマッピング
#define ADDRESS			0x30

#define WHO_AM_I		0x0f
#define I_AM			0x33

#define STATUS_REG_AUX	0x07
#define STATUS_REG		0x27

#define CTRL_REG_1		0x20
#define CTRL_REG_2		0x21
#define CTRL_REG_3		0x22
#define CTRL_REG_4		0x23
#define CTRL_REG_5		0x24
#define CTRL_REG_6		0x25

#define OUT_X_L			0x28
#define OUT_X_H			0x29
#define OUT_Y_L			0x2a
#define OUT_Y_H			0x2b
#define OUT_Z_L			0x2c
#define OUT_Z_H			0x2d
