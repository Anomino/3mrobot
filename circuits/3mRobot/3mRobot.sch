EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA328P-P IC?
U 1 1 570A1051
P 4150 5150
F 0 "IC?" H 3400 6400 50  0000 L BNN
F 1 "ATMEGA328P-P" H 4550 3750 50  0000 L BNN
F 2 "DIL28" H 4150 5150 50  0000 C CIN
F 3 "" H 4150 5150 50  0000 C CNN
	1    4150 5150
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X04 P?
U 1 1 570A1133
P 5750 1250
F 0 "P?" H 5750 1500 50  0000 C CNN
F 1 "CONN_02X04" H 5750 1000 50  0000 C CNN
F 2 "" H 5750 50  50  0000 C CNN
F 3 "" H 5750 50  50  0000 C CNN
	1    5750 1250
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X07 P?
U 1 1 570A11A8
P 7550 1400
F 0 "P?" H 7550 1800 50  0000 C CNN
F 1 "CONN_02X07" V 7550 1400 50  0000 C CNN
F 2 "" H 7550 200 50  0000 C CNN
F 3 "" H 7550 200 50  0000 C CNN
	1    7550 1400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
